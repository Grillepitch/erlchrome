%%%-------------------------------------------------------------------
%% @doc erlchrome top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(erlchrome_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-define(CHILD_WORKER(M, F, A), {M, {M, F, A},
			permanent, 5000, worker, [M]}).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    {ok, {{one_for_all, 0, 1}, [
    	?CHILD_WORKER(erlchrome_srv, start_link, [])
    ]}}.

%%====================================================================
%% Internal functions
%%====================================================================
