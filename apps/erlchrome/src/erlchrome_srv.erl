-module(erlchrome_srv).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {conn_pid, chrome_pid}).

-define(HOST_STR, "localhost").
-define(PREFIX, <<"ws://localhost:9222">>).
-define(TARGET_URL, <<"https://pro.coinbase.com/trade/BTC-USD">>).
-define(TABLE, price).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->	
	gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% gen_server.

init([]) ->

	{ok, ChromePath} = application:get_env(chrome_path),
	{ok, ChromePort} = application:get_env(chrome_port),

	ChromeRunCommand = ChromePath ++ " --remote-debugging-port=" ++ integer_to_list(ChromePort),

	{ok, ChromePid, _OsPid} = exec:run_link(ChromeRunCommand, []),

	io:format("ChromePid: ~p~n", [ChromePid]),

	{ok, ConnPid} = gun:open(?HOST_STR, ChromePort),

	io:format("ConnPid: ~p~n", [ConnPid]),

    erlang:send_after(10 * 1000, self(), start),

    self() ! {print, ets},

	{ok, #state{conn_pid = ConnPid, chrome_pid = ChromePid}}.


handle_call(_Request, _From, State) ->
	{reply, ignored, State}.

handle_cast(_Msg, State) ->
	{noreply, State}.



handle_info({send, Msg}, #state{conn_pid = ConnPid} = State) ->

	io:format("Send msg: ~p~n", [Msg]),

	gun:ws_send(ConnPid, {text, jsx:encode(Msg)}),

	{noreply, State};


handle_info({gun_ws, _ConnPid, _, {text, Msg}}, State) ->

	process_msg(jsx:decode(Msg)),

	% io:format("Got msg: ~p~n", [Msg]),

	{noreply, State};


handle_info({gun_upgrade, ConnPid, _, [<<"websocket">>], _}, #state{conn_pid = ConnPid} = State) ->

	io:format("Upgraded to WS!~n", []),

	self() ! {send, [{<<"id">>, 1}, {<<"method">>, <<"Page.navigate">>}, 
				{<<"params">>, [{<<"url">>, <<"https://pro.coinbase.com/trade/BTC-USD">>}]}]},

	self() ! {send, [{<<"id">>, 2}, {<<"method">>, <<"Network.enable">>}]},

	{noreply, State};

handle_info({gun_up, _, _ConnPid, _}, State) ->
	io:format("Gun Up~n", []),
	{noreply, State};

handle_info(start, #state{conn_pid = ConnPid} = State) ->

	io:format("Started!~n", []),

	_Path =
	case get_new_tab(ConnPid) of
		already_opened 	-> ok;
		Path  			-> Path, gun:ws_upgrade(ConnPid, Path)
	end,

	io:format("'New Tab' Path: ~p~n", [_Path]),

	{noreply, State};


handle_info({print, ets}, State) ->
	
	io:format("ETS: ~p~n", [ets:tab2list(?TABLE)]),

	erlang:send_after(5000, self(), {print, ets}),

	{noreply, State};

handle_info(_Info, State) ->
	{noreply, State}.



terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.


%%%%%%%%%%%%%% INTERNAL %%%%%%%%%%%%%%%

get_new_tab(ConnPid) ->

 	StreamRef = gun:get(ConnPid, "/json", [{<<"accept">>, "application/json"}]),

 	JsonBody =
 	case gun:await(ConnPid, StreamRef) of
 		{response, _, 200, _} ->
 			case gun:await_body(ConnPid, StreamRef) of
 				{ok, _JsonBody} 	-> _JsonBody;
 				_ 			-> error
 			end;
 		_ -> 
 			error
 	end,

 	[Body] = jsx:decode(JsonBody),

 	Path = find_path(Body),

 	binary_to_list(Path).



find_path([_, _, _, _, {_, <<"page">>}, {<<"url">>, <<"chrome://newtab/">>}, {_, Url}]) ->

	[_, Path] = binary:split(Url, [?PREFIX]),

	Path.


process_msg([{<<"method">>,<<"Network.webSocketFrameReceived">>}, {<<"params">>, Params}]) ->
	Resp = get_value(<<"response">>, Params),
	JsonPayload = get_value(<<"payloadData">>, Resp),
	Payload = jsx:decode(JsonPayload),
	save_ticker(Payload);
process_msg(_) ->
	skip.


save_ticker(Payload) ->
	case get_value(<<"type">>, Payload) of
		<<"ticker">> ->
			ProductId = get_value(<<"product_id">>, Payload),
			Price 	  = get_value(<<"price">>, Payload),
			ets:insert(?TABLE, {ProductId, Price});
		_ 			 ->
			skip
	end.


get_value(Key, List)->
    get_value(Key, List, undefined).
get_value(Key, List, Default)->
    case lists:keyfind(Key, 1, List) of
        {_, Val} -> Val;
        _        -> Default
    end.
